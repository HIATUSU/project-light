﻿using UnityEngine;
using System;
using System.Globalization;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class TimerLive : MonoBehaviour
{
    public Text textTime, textLive;
    public float hora;
    public float minutos;
    public float segundos;

    public string retencionName;

    public PlayFabLogin playFab;

    double esperaMs;
    double tiempoActual;
    double totalSecs;

    bool isInit = false, isFin = false;

    private int maxLives = 5;
    private bool fullLive;
    private int contador = 0;

    private void Start()
    {
        textLive.text = PlayerPrefs.GetInt("lives").ToString();

        if (PlayerPrefs.GetInt("lives") >= maxLives) fullLive = true;

        PlayerPrefs.SetString("sumTotal", 0.ToString());

        bool iniciar = false;
        textTime.text = "CARGANDO...";
        if (PlayerPrefs.HasKey(retencionName))
        {
            totalSecs = double.Parse(PlayerPrefs.GetString(retencionName));

            totalSecs += double.Parse(PlayerPrefs.GetString("sumTotal"));

            if (totalSecs < 0)
            {
                textTime.text = "";
                isFin = true;
                return;
            }
        }
        else iniciar = true;

        StartCoroutine(GetTimer(iniciar));
    }
    public void Reset()
    {
        isInit = false;
        StartCoroutine(GetTimer(true));

        textLive.text = PlayerPrefs.GetInt("lives").ToString();
    }

    void Update()
    {
        if (!isFin && isInit)
        {
            tiempoActual += Time.unscaledDeltaTime;

            var secondsLeft = totalSecs - tiempoActual;

            if (secondsLeft < 0)
            {
                int newLife = (PlayerPrefs.GetInt("lives"));

                if (PlayerPrefs.GetInt("lives") < maxLives)
                {
                    for (int i = 0; i < maxLives; i++)
                    {
                        double condition = -(minutos * 60.0f * (maxLives - i - 1));

                        if (secondsLeft < condition)
                        {
                            newLife++;

                            PlayerPrefs.SetInt("lives", newLife);

                            playFab.AddLives(1);

                            textLive.text = newLife.ToString();

                            contador++;

                            if (PlayerPrefs.GetInt("lives") >= maxLives)
                            {
                                isFin = true;
                                textTime.text = "";

                                PlayerPrefs.SetString(retencionName, "-1");

                                PlayerPrefs.SetString("sumTotal", 0.0f.ToString());

                                fullLive = true;

                                contador = 0;

                                return;
                            }
                            else
                            {
                                if (i == 4)
                                {
                                    double newTotal = (-(secondsLeft) - (minutos * 60.0f * (contador - 1)));

                                    PlayerPrefs.SetString("sumTotal", newTotal.ToString());

                                    contador = 0;

                                    Reset();
                                }
                            }
                        }
                    }

                }
                else if(PlayerPrefs.GetInt("lives") >= maxLives)
                {
                    isFin = true;
                    textTime.text = "";

                    PlayerPrefs.SetString(retencionName, "-1");

                    fullLive = true;

                    PlayerPrefs.SetString("sumTotal", 0.0f.ToString());

                    contador = 0;

                    return;
                }
            }
            string r = "";

            secondsLeft = Math.Ceiling(secondsLeft);

            r += ((ulong)secondsLeft / 3600).ToString() + "h";
            secondsLeft -= ((ulong)secondsLeft / 3600) * 3600;

            r += ((ulong)secondsLeft / 60).ToString("00") + "m";

            r += ((ulong)secondsLeft % 60).ToString("00") + "s";
            textTime.text = r;
        }
    }

    public void PlayButton()
    {
        if (fullLive)
        {
            LivesManager();
        }

        textLive.text = PlayerPrefs.GetInt("lives").ToString();
    }

    IEnumerator GetTimer(bool init = false)
    {
        UnityWebRequest www = UnityWebRequest.Get("https://www.google.es");
        yield return www.SendWebRequest();
        string hoy = www.GetResponseHeader("date");
        double tiempo;
        if (hoy == null) Debug.Log("EROR SERVIDOR");
        else
        {
            DateTime dt = DateTime.ParseExact(hoy,
                "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                CultureInfo.InvariantCulture.DateTimeFormat,
                DateTimeStyles.AssumeUniversal);

            tiempo = dt.Ticks / TimeSpan.TicksPerMillisecond;

            tiempoActual = tiempo / 1000.0f;
            if (init)
            {
                esperaMs = ((hora * 60.0f) + minutos) * 60000.0f;
                totalSecs = tiempo + esperaMs;
                totalSecs = totalSecs / 1000.0f;
                totalSecs += segundos;

                totalSecs -= double.Parse(PlayerPrefs.GetString("sumTotal"));

                PlayerPrefs.SetString(retencionName, totalSecs.ToString());
            }

            if (!isInit) isInit = true;
            if (isFin) isFin = false;

        }
    }

    private void LivesManager()
    {
        if (PlayerPrefs.GetInt("lives") < maxLives)
        {
            fullLive = false;

            Reset();
        }
    }
}