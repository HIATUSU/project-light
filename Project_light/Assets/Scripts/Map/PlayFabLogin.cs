﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using System.Collections.Generic;
using System;

public class PlayFabLogin : MonoBehaviour
{
    public void Start()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            /*
            Please change the titleId below to your own titleId from PlayFab Game Manager.
            If you have already set the value in the Editor Extensions, this can be skipped.
            */
            PlayFabSettings.staticSettings.TitleId = "42";
        }

        RequestDevice();
    }

    private void OnLoginSuccess(LoginResult result)
    {
        InitialData();

        Debug.Log("Congratulations, you made your first successful API call!");
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.  :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }

    private void RequestDevice()
    {
        #if UNITY_ANDROID
        var androidRequest = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true
        };
        PlayFabClientAPI.LoginWithAndroidDeviceID(androidRequest, OnLoginSuccess, OnLoginFailure);
    #elif UNITY_IOS
        var iosRequest = new LoginWithIOSDeviceIDRequest
        {
            DeviceId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true
        };
        PlayFabClientAPI.LoginWithIOSDeviceID(iosRequest, OnLoginSuccess, OnLoginFailure);
    #else
        var request = new LoginWithCustomIDRequest { CustomId = "GettingStartedGuide", CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    #endif
    }

    private void InitialData()
    {
        GetTitleDataRequest requestLives = new GetTitleDataRequest
        {
            Keys = new List<string>() { "InitialUserDataLives" }
        };
        PlayFabClientAPI.GetTitleData(requestLives, resultCallback: dataResult =>
        {

        }
        , errorCallback: error =>
        {

        }
        );

        GetTitleDataRequest requestLevel = new GetTitleDataRequest
        {
            Keys = new List<string>() { "PlayerLevelsComplete" }
        };
        PlayFabClientAPI.GetTitleData(requestLevel, resultCallback: dataResult =>
        {

        }
        , errorCallback: error =>
        {

        }
        );


    }

    public void AddLives(int lives)
    {
        PlayFabClientAPI.AddUserVirtualCurrency(
    new AddUserVirtualCurrencyRequest { Amount = lives, VirtualCurrency = "SC" },
    resultCallback: result => {
    }, errorCallback: error => { });

    }

    public void SubstractLives(int lives)
    {
        PlayFabClientAPI.SubtractUserVirtualCurrency(
        new SubtractUserVirtualCurrencyRequest { Amount = lives, VirtualCurrency = "SC" },
        resultCallback: result => {
        }, errorCallback: error => { });
    }

    public void AddLevel()
    {
        PlayFabClientAPI.AddUserVirtualCurrency(
        new AddUserVirtualCurrencyRequest { Amount = 1, VirtualCurrency = "LV" },
        resultCallback: result => {
            PlayerPrefs.SetInt("level", result.Balance);
        }, errorCallback: error => { });

    }
}

[Serializable]
public class InitialUserDataLives
{
    public int lives;
    public int maxLives;
}

[Serializable]
public class PlayerLevelsComplete
{
    public int levelComplete;
}