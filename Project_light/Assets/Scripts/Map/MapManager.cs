﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour
{
    public GameObject levelsPanel, eventsPanel, shopPanel, livePanel, askFriendPanel, profilePanel, leaderboardPanel, returnPanel;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void LevelStart(int level)
    {
        PlayerPrefs.SetInt("actualLevel", level);

        SceneManager.LoadScene(2);
    }

    public void Lives()
    {
        livePanel.SetActive(true);

        returnPanel.SetActive(true);
    }

    public void Profile()
    {
        profilePanel.SetActive(true);

        returnPanel.SetActive(true);
    }

    public void Map()
    {
        levelsPanel.SetActive(true);

        eventsPanel.SetActive(false);

        shopPanel.SetActive(false);
    }

    public void Event()
    {
        levelsPanel.SetActive(false);

        eventsPanel.SetActive(true);

        shopPanel.SetActive(false);
    }

    public void Shop()
    {
        levelsPanel.SetActive(false);

        eventsPanel.SetActive(false);

        shopPanel.SetActive(true);
    }

    public void ReturnPanel()
    {
        livePanel.SetActive(false);

        profilePanel.SetActive(false);

        leaderboardPanel.SetActive(false);

        askFriendPanel.SetActive(false);

        returnPanel.SetActive(false);
    }

    public void AskFriends()
    {
        livePanel.SetActive(false);

        askFriendPanel.SetActive(true);
    }

    public void LeaderBoard()
    {
        profilePanel.SetActive(false);

        leaderboardPanel.SetActive(true);
    }
}
