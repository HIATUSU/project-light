﻿using GameFlow;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int scene, totalP;

    private bool num1, num2, num3, num4, num0;

    public Text timeTxt;

    public float timer;

    private float timeIS = 0.0f;
    [HideInInspector]
    public bool pause = true;

    private GameObject canvasGameplay;

    public GameObject canvasWin, canvasLose;

    public Text hotColdTxt, easyOneTxt;

    public Material red;

    private LevelGenerator levelG;

    private Obstacle obstacle;

    private RayDetector rayD;

    private PlayFabLogin playFab;

    public List<GameObject> levels;

    private List<int> wrongObjects = new List<int>();

    [HideInInspector]
    public bool hotCold, noDetect;

    [HideInInspector]
    public int percent;

    private void Awake()
    {
        int levelNum = (PlayerPrefs.GetInt("actualLevel") - 1);

        levels[levelNum].SetActive(true);

        Debug.Log(levelNum);

        PlayerPrefs.SetInt("hotCold", 5);

        PlayerPrefs.SetInt("easyOne", 5);

        hotCold = false;

        hotColdTxt.text = PlayerPrefs.GetInt("hotCold").ToString();

        easyOneTxt.text = PlayerPrefs.GetInt("easyOne").ToString();
    }

    void Start()
    {
        StartCoroutine("TakeValues");

        noDetect = true;

        num1 = false;

        num2 = false;

        num3 = false;

        num4 = false;

        num0 = false;

        timeIS = timer;

        Timer(timer);
    }

    private void Update()
    {
        if (!pause)
        {
            timeIS -= Time.deltaTime;

            Timer(timeIS);
        }

        if(timeIS <= 0)
        {
            pause = true;

            timeTxt.text = "";

            Lose();
        }
    }

    public void MoreTime(int time)
    {
        timeIS += time;
    }

    public void MoreMoves(int moves)
    {
        rayD.MoreMoves(moves);
    }

    private void Timer(float time)
    {
        int minutes = 0;

        int seconds = 0;

        string textTime;

        if (timeIS < 0) timeIS = 0;

        minutes = (int)timeIS / 60;
        seconds = (int)timeIS % 60;
        textTime = minutes.ToString("00") + ":" + seconds.ToString("00");

        timeTxt.text = textTime;
    }

    private IEnumerator TakeValues()
    {
        yield return new WaitForSeconds(0.2f);

        scene = SceneManager.GetActiveScene().buildIndex;

        playFab = transform.gameObject.GetComponent<PlayFabLogin>();

        levelG = GameObject.Find("LevelManager").GetComponent<LevelGenerator>();

        rayD = GameObject.Find("LevelManager").GetComponent<RayDetector>();

        obstacle = GameObject.Find("Obstacle").GetComponent<Obstacle>();

        canvasGameplay = GameObject.Find("CanvasGameplay");

        totalP = ((levelG.QuadLenght + levelG.QuadVariation) * levelG.QuadLenght);

        yield return new WaitForSeconds(0.01f);

        for (int i = 0; i < levelG.PiecePos; i++)
        {
            wrongObjects.Add(i + 1);
        }

        yield return new WaitForSeconds(0.3f);

        for (int i = 0; i < obstacle.totalPosPiece.Count; i++)
        {
            wrongObjects.Remove(obstacle.totalPosPiece[i]);
        }
    }

    public void Boosterbutton(int booster)
    {
        if(booster == 1)
        {
            if (PlayerPrefs.GetInt("hotCold") > 0)
            {
                if (!hotCold)
                {
                    hotCold = true;

                    int newValue = PlayerPrefs.GetInt("hotCold") - 1;

                    PlayerPrefs.SetInt("hotCold", newValue);

                    hotColdTxt.text = PlayerPrefs.GetInt("hotCold").ToString();
                }
                else
                {
                    hotCold = false;

                    int newValue = PlayerPrefs.GetInt("hotCold") + 1;

                    PlayerPrefs.SetInt("hotCold", newValue);

                    hotColdTxt.text = PlayerPrefs.GetInt("hotCold").ToString();
                }
            }
            else
            {
                transform.gameObject.GetComponent<CanvasManager>().Booster();
            }
        }
        else if(booster == 2)
        {
            if (PlayerPrefs.GetInt("easyOne") > 0)
            {
                int newValue = PlayerPrefs.GetInt("easyOne") - 1;

                PlayerPrefs.SetInt("easyOne", newValue);

                easyOneTxt.text = PlayerPrefs.GetInt("easyOne").ToString();

                Debug.Log(wrongObjects.Count);

                EasyOne();
            }
            else
            {
                transform.gameObject.GetComponent<CanvasManager>().Booster();
            }
        }
    }

    private void EasyOne()
    {
        if(wrongObjects.Count > 0)
        {
            int rndNum = Random.Range(0, wrongObjects.Count);

            int newWrong = wrongObjects[rndNum];

            Debug.Log(rndNum);

            levelG.Pieces[newWrong - 1].GetComponent<MeshRenderer>().material = red;

            wrongObjects.Remove(newWrong);
        }
        else
        {

        }
    }

    public void Push()
    {
        if (totalP == percent)
        {
            PushDetector();

            if(num1 && num2 && num3 && num4 && num0)
            {
                Win();
            }
            else
            {
                Lose();
            }
        }
        else
        {
            Lose();
        }
    }

    public void Retry()
    {
        SceneManager.LoadScene(scene);
    }

    private void PushDetector()
    {
        if (obstacle.piecesNum1.Count > 0)
        {
            for (int i = 0; i < obstacle.piecesNum1.Count; i++)
            {
                NumDetector numD = obstacle.piecesNum1[i].GetComponentInParent<NumDetector>();

                if (!numD.caseBool)
                {
                    num1 = false;

                    break;
                }
                else
                {
                    num1 = true;
                }
            }
        }
        else
        {
            num1 = true;
        }

        if(obstacle.piecesNum2.Count > 0)
        {
            for (int i = 0; i < obstacle.piecesNum2.Count; i++)
            {
                NumDetector numD = obstacle.piecesNum2[i].GetComponentInParent<NumDetector>();

                if (!numD.caseBool)
                {
                    num2 = false;

                    break;
                }
                else
                {
                    num2 = true;
                }
            }
        }
        else
        {
            num2 = true;
        }

        if(obstacle.piecesNum3.Count > 0)
        {
            for (int i = 0; i < obstacle.piecesNum3.Count; i++)
            {
                NumDetector numD = obstacle.piecesNum3[i].GetComponentInParent<NumDetector>();

                if (!numD.caseBool)
                {
                    num3 = false;

                    break;
                }
                else
                {
                    num3 = true;
                }
            }
        }
        else
        {
            num3 = true;
        }

        if(obstacle.piecesNum4.Count > 0)
        {
            for (int i = 0; i < obstacle.piecesNum4.Count; i++)
            {
                NumDetector numD = obstacle.piecesNum4[i].GetComponentInParent<NumDetector>();

                if (!numD.caseBool)
                {
                    num4 = false;

                    break;
                }
                else
                {
                    num4 = true;
                }
            }
        }
        else
        {
            num4 = true;
        }      
        
        if(obstacle.piecesNum0.Count > 0)
        {
            for (int i = 0; i < obstacle.piecesNum0.Count; i++)
            {
                NumDetector numD = obstacle.piecesNum0[i].GetComponentInParent<NumDetector>();

                if (!numD.caseBool)
                {
                    num0 = false;

                    break;
                }
                else
                {
                    num0 = true;
                }
            }
        }
        else
        {
            num0 = true;
        }

    }
    public void Win()
    {
        canvasGameplay.SetActive(false);

        canvasWin.SetActive(true);

        if (PlayerPrefs.GetInt("level") > 1)
        {
            playFab.AddLevel();
        }
    }

    public void Lose()
    {
        canvasGameplay.SetActive(false);

        canvasLose.SetActive(true);
    }
}
