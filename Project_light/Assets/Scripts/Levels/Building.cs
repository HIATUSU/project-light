﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class Building : MonoBehaviour
{
    public int typeB;

    public List<GameObject> buildings;

    private void OnEnable()
    {
        int ran = Random.Range(1, (buildings.Count + 1));

        ran--;

        GameObject newBuild = Instantiate(buildings[ran], transform.position, Quaternion.Euler(-90, 0, 0));
    }

    private void OnDisable()
    {
        
    }
}
