﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RayDetector : MonoBehaviour
{
    public LayerMask Piece;

    private Text bombTxt;

    public int bombs;

    private GameManager gameM;

    public List<int> solutions;

    private void Start()
    {
        bombTxt = GameObject.Find("Bombs_text").GetComponent<Text>();

        gameM = GameObject.Find("GameManager").GetComponent<GameManager>();

        bombTxt.text = bombs.ToString();
    }

    public void MoreMoves(int moves)
    {
        bombs += moves;

        bombTxt.text = bombs.ToString();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Piece))
            {

                if (gameM.hotCold)
                {
                    hit.transform.GetComponent<Boosters>().HotCold();

                    gameM.hotCold = false;
                }
                else if (gameM.noDetect)
                {

                }
                else
                {
                    if (bombs > 0)
                    {
                        hit.transform.GetComponent<ExplosiveController>().activated = !hit.transform.GetComponent<ExplosiveController>().activated;

                        bombs--;

                        if (hit.transform.GetComponent<ExplosiveController>().activated == false)
                        {
                            bombs += 2;

                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffUp();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffDown();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffLeft();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffRight();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOff();
                        }
                    }
                    else
                    {
                        if (hit.transform.GetComponent<ExplosiveController>().activated == true)
                        {
                            bombs++;

                            hit.transform.GetComponent<ExplosiveController>().activated = false;

                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffUp();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffDown();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffLeft();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOffRight();
                            hit.transform.GetComponent<ExplosiveController>().SetExplosiveOff();
                        }
                    }

                    bombTxt.text = bombs.ToString();
                }      
            }
        }
    }
}
