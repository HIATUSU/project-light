﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public int QuadLenght, QuadVariation, PiecePos, CurrentPPice;
    public GameObject LevelParent, LevelSection;
    private Vector3 SectionInitPos, SectionNextPos;

    [HideInInspector]
    public List<GameObject> Pieces = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        //Pieces[nombre = su numero de posicion + (QuadLenght + QuadVariation)] = casilla de encima;
        //Pieces[nombre = su numero de posicion - (QuadLenght + QuadVariation)] = casilla de debajo;
        //Pieces[nombre = su numero de posicion + 1] = casilla de la derecha;
        //Pieces[nombre = su numero de posicion - 1] = casilla de la izquierda
        SectionInitPos = new Vector3(-1.1f, 0, 0);
        SectionNextPos = SectionInitPos;
        for (int i = 0; i < QuadLenght; i++)
        {
            SectionNextPos.x = SectionInitPos.x;
            SectionNextPos.z += 1.1f;
            for (int y = 0; y < QuadLenght + QuadVariation; y++)
            {
                PiecePos += 1;
                SectionNextPos.x += 1.1f;
                GameObject newPiece = Instantiate(LevelSection, SectionNextPos, Quaternion.Euler(90, 0, 0));
                newPiece.name = PiecePos.ToString();
                newPiece.transform.parent = LevelParent.transform;
                Pieces.Add(newPiece);              
            }
        }
        
    }
    
}
