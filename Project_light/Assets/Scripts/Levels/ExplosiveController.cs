﻿using GameFlow;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveController : MonoBehaviour
{
    private LevelGenerator leveruGenerator;
    private int ColindantesUp, ColindantesDown, ColindantesLeft, ColindantesRight, R, L, Sumatorio;
    [HideInInspector]
    public bool activated;
    // Start is called before the first frame update
    void Start()
    {
        leveruGenerator = GameObject.Find("LevelManager").GetComponent<LevelGenerator>();
        ColindantesUp = int.Parse(transform.name) - 1;
        activated = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated)
        {
            SetExplosiveOnUp();
            SetExplosiveOnDown();
            SetExplosiveOnLeft();
            SetExplosiveOnRight();
            SetExplosiveOn();
        }
    }

    private void SetExplosiveOn()
    {
        int casilla = int.Parse(transform.name) - 1;

        leveruGenerator.Pieces[casilla].transform.GetChild(0).gameObject.SetActive(true);

    }

    public void SetExplosiveOff()
    {
        int casilla = int.Parse(transform.name) - 1;

        leveruGenerator.Pieces[casilla].transform.GetChild(0).gameObject.SetActive(false);
    }
    public void SetExplosiveOnUp()
    {
        ColindantesUp = int.Parse(transform.name) - 1;
        for (int i = 0; i < (leveruGenerator.Pieces.Count); i++)
        {
            if (ColindantesUp < (leveruGenerator.Pieces.Count) - (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation))
            {
                ColindantesUp += leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;

                if (leveruGenerator.Pieces[ColindantesUp].transform.childCount > 2)
                {
                    break;
                }
                else
                {
                    leveruGenerator.Pieces[ColindantesUp].transform.GetChild(1).gameObject.SetActive(true);
                }
            }
        }
    }
    public void SetExplosiveOnDown()
    {
        ColindantesDown = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            ColindantesDown -= leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;

            if (ColindantesDown < 0)
            {
                break;
            }

            if (leveruGenerator.Pieces[ColindantesDown].transform.childCount > 2)
            {
                break;
            }
            else
            {
                leveruGenerator.Pieces[ColindantesDown].transform.GetChild(1).gameObject.SetActive(true);
            }
        }
    }
    public void SetExplosiveOffUp()
    {
        ColindantesUp = int.Parse(transform.name) - 1;
        for (int i = 0; i < (leveruGenerator.Pieces.Count); i++)
        {
            if(ColindantesUp < (leveruGenerator.Pieces.Count) - (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation))
            {
                ColindantesUp += leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;

                leveruGenerator.Pieces[ColindantesUp].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
    }
    public void SetExplosiveOffDown()
    {
        ColindantesDown = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            ColindantesDown -= leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;

            if (ColindantesDown < 0)
            {
                break;
            }

            leveruGenerator.Pieces[ColindantesDown].transform.GetChild(1).gameObject.SetActive(false);
        }
    }
    public void SetExplosiveOnLeft()
    {
        R = 0;
        L = 0;
        ColindantesLeft = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            R += 1;
            if(R == leveruGenerator.QuadLenght + leveruGenerator.QuadVariation)
            {
                L += 1;
                R = 0;
            }
            if(i == int.Parse(transform.name) - 1)
            {
                Sumatorio = (int.Parse(transform.name) - 1) - ((leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) * L);
                if (int.Parse(transform.name) == (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) * L || int.Parse(transform.name) == (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation))
                {
                    Sumatorio = leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;
                }
                break;
            }
        }
        for (int i = 0; i < Sumatorio; i++)
        {
            if(ColindantesLeft > 0)
            {
                ColindantesLeft -= 1;

                if (leveruGenerator.Pieces[ColindantesLeft].transform.childCount > 2)
                {
                    break;
                }

                leveruGenerator.Pieces[ColindantesLeft].transform.GetChild(1).gameObject.SetActive(true);
                /*if (leveruGenerator.Pieces[ColindantesLeft].transform.childCount > 2)
                {
                    break;
                }*/
            }          
        }
    }
    public void SetExplosiveOffLeft()
    {
        R = 0;
        L = 0;
        ColindantesLeft = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            R += 1;
            if (R == leveruGenerator.QuadLenght + leveruGenerator.QuadVariation)
            {
                L += 1;
                R = 0;
            }
            if (i == int.Parse(transform.name) - 1)
            {
                Sumatorio = (int.Parse(transform.name) - 1) - ((leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) * L);
                if (int.Parse(transform.name) == (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) * L || int.Parse(transform.name) == (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation))
                {
                    Sumatorio = leveruGenerator.QuadLenght + leveruGenerator.QuadVariation;
                }
                break;
            }
        }
        for (int i = 0; i < Sumatorio; i++)
        {
            if (ColindantesLeft > 0)
            {
                ColindantesLeft -= 1;
                leveruGenerator.Pieces[ColindantesLeft].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
    }
    public void SetExplosiveOnRight()
    {
        R = 0;
        L = 0;
        ColindantesRight = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            R += 1;
            if (R == leveruGenerator.QuadLenght + leveruGenerator.QuadVariation)
            {
                L += 1;
                R = 0;
            }
            if (i == int.Parse(transform.name) - 1)
            {

                break;
            }
        }
        for (int i = 0; i < (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) - Sumatorio - 1; i++)
        {
            ColindantesRight += 1;

            if (leveruGenerator.Pieces[ColindantesRight].transform.childCount > 2)
            {
                break;
            }

            leveruGenerator.Pieces[ColindantesRight].transform.GetChild(1).gameObject.SetActive(true);

            /*if (leveruGenerator.Pieces[ColindantesRight].transform.childCount > 2)
            {
                break;
            }*/
        }
    }
    public void SetExplosiveOffRight()
    {
        R = 0;
        L = 0;
        ColindantesRight = int.Parse(transform.name) - 1;
        for (int i = 0; i < leveruGenerator.Pieces.Count; i++)
        {
            R += 1;
            if (R == leveruGenerator.QuadLenght + leveruGenerator.QuadVariation)
            {
                L += 1;
                R = 0;
            }
            if (i == int.Parse(transform.name) - 1)
            {

                break;
            }
        }
        for (int i = 0; i < (leveruGenerator.QuadLenght + leveruGenerator.QuadVariation) - Sumatorio; i++)
        {
            leveruGenerator.Pieces[ColindantesRight].transform.GetChild(1).gameObject.SetActive(false);
            ColindantesRight += 1;
        }
    }
}
