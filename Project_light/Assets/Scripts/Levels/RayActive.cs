﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayActive : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine("OnActive");
    }

    private IEnumerator OnActive()
    {
        yield return new WaitForSeconds(0.02f);

        transform.gameObject.GetComponentInParent<BoxCollider>().enabled = false;
    }
}
