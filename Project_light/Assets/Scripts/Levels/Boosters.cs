﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class Boosters : MonoBehaviour
{
    private RaycastHit hitUp, hitDown, hitLeft, hitRight;

    public Material red;

    [HideInInspector]
    public GameObject objectUp, objectDown, objectLeft, objectRight;

    private List<GameObject> RangeObjects = new List<GameObject>();

    private List<GameObject> objectswrong = new List<GameObject>();

    private RayDetector rayD;

    void Start()
    {
        RayCastFour();

        rayD = GameObject.Find("LevelManager").GetComponent<RayDetector>();
    }

    void Update()
    {
        
    }

    public void HotCold()
    {
        RangeObjects.Clear();

        if (transform.gameObject.GetComponent<ExplosiveController>().activated)
        {
            RangeObjects.Add(transform.gameObject);
        }

        if (objectUp != null)
        {
            if (objectUp.GetComponent<ExplosiveController>().activated)
            {
                RangeObjects.Add(objectUp);
            }

            if(objectUp.GetComponent<Boosters>().objectLeft != null)
            {
                RangeObjects.Add(objectUp.GetComponent<Boosters>().objectLeft);
            }

            if (objectUp.GetComponent<Boosters>().objectRight != null)
            {
                RangeObjects.Add(objectUp.GetComponent<Boosters>().objectRight);
            }
        }

        if (objectDown != null)
        {
            if (objectDown.GetComponent<ExplosiveController>().activated)
            {
                RangeObjects.Add(objectDown);
            }

            if (objectDown.GetComponent<Boosters>().objectLeft != null)
            {
                RangeObjects.Add(objectDown.GetComponent<Boosters>().objectLeft);
            }

            if (objectDown.GetComponent<Boosters>().objectRight != null)
            {
                RangeObjects.Add(objectDown.GetComponent<Boosters>().objectRight);
            }
        }

        if (objectLeft != null)
        {
            if (objectLeft.GetComponent<ExplosiveController>().activated)
            {
                RangeObjects.Add(objectLeft);
            }
        }

        if (objectRight != null)
        {
            if (objectRight.GetComponent<ExplosiveController>().activated)
            {
                RangeObjects.Add(objectRight);
            }
        }

        StartCoroutine("HotColdDetect");
    }

    private IEnumerator HotColdDetect()
    {
        yield return new WaitForSeconds(0.1f);

        for (int i = 0; i < RangeObjects.Count; i++)
        {
            for (int e = 0; e < rayD.solutions.Count; e++)
            {
                string name = rayD.solutions[e].ToString();

                if (name != RangeObjects[i].name)
                {
                    if(e == (rayD.solutions.Count - 1))
                    {
                        RangeObjects[i].transform.GetChild(0).GetComponent<MeshRenderer>().material = red;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    private void RayCastFour()
    {
        if (Physics.Raycast(transform.position, Vector3.forward, out hitUp, 2f))
        {
            if (hitUp.collider.gameObject != null)
            {
                objectUp = hitUp.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, -Vector3.forward, out hitDown, 2f))
        {
            if (hitDown.collider.gameObject != null)
            {
                objectDown = hitDown.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, Vector3.right, out hitRight, 2f))
        {
            if (hitRight.collider.gameObject != null)
            {
                objectRight = hitRight.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, -Vector3.right, out hitLeft, 2f))
        {
            if (hitLeft.collider.gameObject != null)
            {
                objectLeft = hitLeft.collider.gameObject;
            }
        }
    }

    private void RayCastSides()
    {
        if (Physics.Raycast(transform.position, Vector3.right, out hitRight, 2f))
        {
            if (hitRight.collider.gameObject != null)
            {
                objectRight = hitRight.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, -Vector3.right, out hitLeft, 2f))
        {
            if (hitLeft.collider.gameObject != null)
            {
                objectLeft = hitLeft.collider.gameObject;
            }
        }
    }
}
