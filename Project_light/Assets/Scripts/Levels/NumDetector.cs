﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class NumDetector : MonoBehaviour
{
    private Number number;

    private int num;

    private bool up, down, left, right;

    private RaycastHit hitUp, hitDown, hitLeft, hitRight;

    private GameObject objectUp, objectDown, objectLeft, objectRight;

    private bool activeDetection = false;

    public  bool caseBool = false;
    void Start()
    {
        up = false;
        down = false;
        left = false;
        right = false;
    }

    void Update()
    {
        if (!activeDetection)
        {
            if (transform.childCount > 2)
            {
                if (transform.GetChild(2).gameObject.GetComponent<Number>())
                {
                    number = transform.GetChild(2).gameObject.GetComponent<Number>();

                    num = number.BlockNum;

                    RayCast();

                    activeDetection = true;
                }
            }
        }
        else
        {
            NumLogic();

            DetectBool();
        }
    }

    private void NumLogic()
    {
        switch (num)
        {
            case 1:
                CaseOne();

                break;

            case 2:
                CaseTwo();

                break;

            case 3:
                CaseThree();

                break;

            case 4:
                CaseFour();

                break;
            case 0:
                CaseCero();

                break;
        }
    }

    private void CaseOne()
    {
        if(up || down || left || right)
        {
            caseBool = true;

            /*if (up)
            {
                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (down)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (left)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (right)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }
            }*/
        }
        else
        {
            if (caseBool)
            {
                //ActivateObject();

                caseBool = false;
            }
        }  
    }

    private void CaseTwo()
    {
        if(up && down || up && left || up && right || down && left || down && right || left && right)
        {
            caseBool = true;

            /*if(up && down)
            {
                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(up && left)
            {
                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(up && right)
            {
                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(down && left)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (down && right)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(left && right)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }

                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }
            }*/
        }
        else
        {
            if (caseBool)
            {
                //ActivateObject();

                caseBool = false;
            }
        }
    }

    private void CaseThree()
    {
        if(up && down && left || up && down && right || down && right && left || up && right && left)
        {
            caseBool = true;

            /*if(up && down && left)
            {
                if (objectRight != null)
                {
                    objectRight.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(up && down && right)
            {
                if (objectLeft != null)
                {
                    objectLeft.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(down && right && left)
            {
                if (objectUp != null)
                {
                    objectUp.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if(up && right && left)
            {
                if (objectDown != null)
                {
                    objectDown.GetComponent<BoxCollider>().enabled = false;
                }
            }*/
        }
        else
        {
            if (caseBool)
            {
                //ActivateObject();

                caseBool = false;
            }
        }
    }

    private void CaseFour()
    {
        if(up && down && left && right)
        {
            if (!caseBool)
            {
                caseBool = true;
            }
        }
        else
        {
            if (caseBool)
            {
                caseBool = false;
            }
        }
    }

    private void CaseCero()
    {
       if(up || down || left || right)
        {
            if (caseBool)
            {
                caseBool = false;
            }
        }
        else
        {
            if (!caseBool)
            {
                caseBool = true;
            }
        }
    }

    private void ActivateObject()
    {
        if (objectDown != null)
        {
            objectDown.GetComponent<BoxCollider>().enabled = true;
        }

        if (objectLeft != null)
        {
            objectLeft.GetComponent<BoxCollider>().enabled = true;
        }

        if (objectRight != null)
        {
            objectRight.GetComponent<BoxCollider>().enabled = true;
        }

        if (objectUp != null)
        {
            objectUp.GetComponent<BoxCollider>().enabled = true;
        }
    }

    private void DetectBool()
    {
        if(objectUp != null)
        {
            if (objectUp.GetComponent<ExplosiveController>().activated)
            {
                up = true;
            }
            else
            {
                up = false;
            }
        }

        if (objectDown != null)
        {
            if (objectDown.GetComponent<ExplosiveController>().activated)
            {
                down = true;
            }
            else
            {
                down = false;
            }
        }

        if (objectLeft != null)
        {
            if (objectLeft.GetComponent<ExplosiveController>().activated)
            {
                left = true;
            }
            else
            {
                left = false;
            }
        }

        if (objectRight != null)
        {
            if (objectRight.GetComponent<ExplosiveController>().activated)
            {
                right = true;
            }
            else
            {
                right = false;
            }
        }
    }

    private void RayCast()
    {
        if (Physics.Raycast(transform.position, Vector3.forward, out hitUp, 2f))
        {
            if(hitUp.collider.gameObject != null)
            {
                objectUp = hitUp.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, -Vector3.forward, out hitDown, 2f))
        {
            if(hitDown.collider.gameObject != null)
            {
                objectDown = hitDown.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, Vector3.right, out hitRight, 2f))
        {
            if(hitRight.collider.gameObject != null)
            {
                objectRight = hitRight.collider.gameObject;
            }
        }

        if (Physics.Raycast(transform.position, -Vector3.right, out hitLeft, 2f))
        {
            if(hitLeft.collider.gameObject != null)
            {
                objectLeft = hitLeft.collider.gameObject;
            }
        }
    }
}
