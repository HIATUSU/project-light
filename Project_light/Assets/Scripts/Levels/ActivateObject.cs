﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{
    private GameManager gameManager;

    private void OnEnable()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        gameManager.percent++;
    }

    private void OnDisable()
    {
        gameManager.percent--;
    }
}
