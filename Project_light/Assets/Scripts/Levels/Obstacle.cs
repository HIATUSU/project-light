﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public LevelGenerator levelG;

    public GameObject obstacle, obstacleNum1, obstacleNum2, obstacleNum3, obstacleNum4, obstacleNum0;

    public List<int> posObstacle, posObstacleNum1, posObstacleNum2, posObstacleNum3, posObstacleNum4, posObstacleNum0, totalPosPiece;

    [HideInInspector]
    public List<GameObject> piecesNum1, piecesNum2, piecesNum3, piecesNum4, piecesNum0;

    private void Awake()
    {
        piecesNum1.Clear();
        piecesNum2.Clear();
        piecesNum3.Clear();
        piecesNum4.Clear();
        piecesNum0.Clear();
    }

    void Start()
    {
        StartCoroutine("CreateObstacle");

        totalPosPiece.AddRange(posObstacle);

        totalPosPiece.AddRange(posObstacleNum0);

        totalPosPiece.AddRange(posObstacleNum1);

        totalPosPiece.AddRange(posObstacleNum2);

        totalPosPiece.AddRange(posObstacleNum3);

        totalPosPiece.AddRange(posObstacleNum4);

        totalPosPiece.AddRange(levelG.transform.GetComponent<RayDetector>().solutions);

        Debug.Log(totalPosPiece.Count);
    }

    private IEnumerator CreateObstacle()
    {
        yield return new WaitForSeconds(0.2f);

        for(int i = 0; i < posObstacle.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacle[i].ToString());

            GameObject newPiece = Instantiate(obstacle, piece.transform.position, Quaternion.Euler(0,0,0));

            newPiece.transform.parent = piece.transform;
        }

        for (int i = 0; i < posObstacleNum1.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacleNum1[i].ToString());

            GameObject newPiece = Instantiate(obstacleNum1, piece.transform.position, Quaternion.Euler(0, 0, 0));

            newPiece.transform.parent = piece.transform;

            piecesNum1.Add(newPiece);
        }

        for (int i = 0; i < posObstacleNum2.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacleNum2[i].ToString());

            GameObject newPiece = Instantiate(obstacleNum2, piece.transform.position, Quaternion.Euler(0, 0, 0));

            newPiece.transform.parent = piece.transform;

            piecesNum2.Add(newPiece);
        }

        for (int i = 0; i < posObstacleNum3.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacleNum3[i].ToString());

            GameObject newPiece = Instantiate(obstacleNum3, piece.transform.position, Quaternion.Euler(0, 0, 0));

            newPiece.transform.parent = piece.transform;

            piecesNum3.Add(newPiece);
        }

        for (int i = 0; i < posObstacleNum4.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacleNum4[i].ToString());

            GameObject newPiece = Instantiate(obstacleNum4, piece.transform.position, Quaternion.Euler(0, 0, 0));

            newPiece.transform.parent = piece.transform;

            piecesNum4.Add(newPiece);
        }

        for (int i = 0; i < posObstacleNum0.Count; i++)
        {
            GameObject piece = GameObject.Find(posObstacleNum0[i].ToString());

            GameObject newPiece = Instantiate(obstacleNum0, piece.transform.position, Quaternion.Euler(0, 0, 0));

            newPiece.transform.parent = piece.transform;

            piecesNum0.Add(newPiece);
        }
    }
}
