﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class CanvasManager : MonoBehaviour
{
    public GameObject tutorialPanel, boosterPanel, settingsPanel, returnGameplayPanel, quitLevelPanel, startLevelPanel, noLiveTxt;

    public Text levelText;

    public PlayFabLogin playFab;

    private GameManager gameM;

    private void Start()
    {
        //PlayerPrefs.DeleteKey("tutorialDone");

        gameM = transform.gameObject.GetComponent<GameManager>();

        if (PlayerPrefs.GetString("tutorialDone") == "")
        {
            tutorialPanel.SetActive(true);
        }

        levelText.text = "Level" + PlayerPrefs.GetInt("actualLevel").ToString();
    }

    public void Play()
    {
        if(PlayerPrefs.GetInt("lives") > 0)
        {
            StartCoroutine("StartLevel");

            //int newlife = PlayerPrefs.GetInt("lives") - 1;

            //PlayerPrefs.SetInt("lives", newlife);

            //playFab.SubstractLives(1);
        }
        else
        {
            StartCoroutine("NoLiveCondition");
        }
    }

    private IEnumerator StartLevel()
    {
        yield return new WaitForSeconds(0.02f);

        gameM.pause = false;

        gameM.noDetect = false;

        startLevelPanel.SetActive(false);
    }

    private IEnumerator NoLiveCondition()
    {
        yield return new WaitForSeconds(0.01f);

        noLiveTxt.SetActive(true);

        yield return new WaitForSeconds(1f);

        noLiveTxt.SetActive(false);
    }

    public void TutorialBegin()
    {
        tutorialPanel.SetActive(false);

        PlayerPrefs.SetString("tutorialDone","Yes");
    }

    public void Settings()
    {
        settingsPanel.SetActive(true);

        gameM.pause = true;

        returnGameplayPanel.SetActive(true);
    }

    public void Booster()
    {
        boosterPanel.SetActive(true);

        gameM.pause = true;

        returnGameplayPanel.SetActive(true);
    }

    public void ReturnGameplay()
    {
        settingsPanel.SetActive(false);

        boosterPanel.SetActive(false);
        
        returnGameplayPanel.SetActive(false);

        gameM.pause = false;
    }

    public void QuitLevel()
    {
        quitLevelPanel.SetActive(true);

        gameM.pause = true;
    }

    public void KeepPlaying()
    {
        quitLevelPanel.SetActive(false);

        settingsPanel.SetActive(false);

        returnGameplayPanel.SetActive(false);
    }

    public void ReturnToMap()
    {
        SceneManager.LoadScene(1);
    }
}
