﻿using System.Collections;
using UnityEngine;


public class InternetDetection : MonoBehaviour
{
    public GameObject canvasNoInternet;

    private bool detect;

    private void Start()
    {
        detect = false;
    }

    private void LateUpdate()
    {
        if (!detect)
        {
            detect = true;

            StartCoroutine("CheckConection");
        }
    }

    private IEnumerator CheckConection()
    {
        yield return new WaitForSeconds(1f);

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            canvasNoInternet.SetActive(false);
        }
        else canvasNoInternet.SetActive(true);

        detect = false;
    }
}
