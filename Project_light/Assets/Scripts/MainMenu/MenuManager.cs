﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject mainPanel, settingsPanel, returnMain, moreGamesPanel, mainSettings, howToPlay, notifications, freatures, retrieveMPanel, acountPanel;

    private PlayFabLogin playFab;

    private void Start()
    {
        PlayerPrefs.DeleteKey("tutorialDone");

        playFab = transform.gameObject.GetComponent<PlayFabLogin>();

        if (!PlayerPrefs.HasKey("level"))
        {
            PlayerPrefs.SetInt("level", 0);
        }


    }
    public void Play()
    {
        SceneManager.LoadScene(2);
    }

    public void RetrieveMP()
    {
        retrieveMPanel.SetActive(true);

        returnMain.SetActive(true);
    }

    public void Settings()
    {
        settingsPanel.SetActive(true);

        returnMain.SetActive(true);
    }

    public void MoreGames()
    {
        moreGamesPanel.SetActive(true);

        returnMain.SetActive(true);
    }

    public void ReturnMain()
    {
        moreGamesPanel.SetActive(false);

        mainSettings.SetActive(true);

        howToPlay.SetActive(false);

        notifications.SetActive(false);

        freatures.SetActive(false);

        settingsPanel.SetActive(false);

        returnMain.SetActive(false);

        retrieveMPanel.SetActive(false);

        acountPanel.SetActive(false);
    }

    public void MyAcount()
    {
        Debug.Log("Cargar Cuenta");

        acountPanel.SetActive(true);
    }

    public void BackAcount()
    {
        acountPanel.SetActive(false);
    }

    public void HowToPlay()
    {
        mainSettings.SetActive(false);

        howToPlay.SetActive(true);
    }

    public void ReturnMainSettings()
    {
        mainSettings.SetActive(true);

        howToPlay.SetActive(false);

        notifications.SetActive(false);

        freatures.SetActive(false);
    }

    public void Notifications()
    {
        mainSettings.SetActive(false);

        notifications.SetActive(true);
    }

    public void Freatures()
    {
        mainSettings.SetActive(false);

        freatures.SetActive(true);
    }

    public void Web()
    {
        Debug.Log("Abrir pagina web");
    }

    public void Help()
    {
        Debug.Log("Abir ayuda");
    }
}
